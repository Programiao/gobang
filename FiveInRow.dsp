# Microsoft Developer Studio Project File - Name="FiveInRow" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=FiveInRow - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "FiveInRow.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "FiveInRow.mak" CFG="FiveInRow - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "FiveInRow - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "FiveInRow - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "FiveInRow - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x804 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x804 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "FiveInRow - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "\tmp\Wuziqi\Debug"
# PROP Intermediate_Dir "\tmp\Wuziqi\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "." /I "GameLogical" /I "GameData" /I "GameUI" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x804 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x804 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"../../DISTRIBUTE/������.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "FiveInRow - Win32 Release"
# Name "FiveInRow - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\FiveInRow.rc
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=".\res\040-Tower02.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\aboutus.bmp
# End Source File
# Begin Source File

SOURCE=.\res\black.bmp
# End Source File
# Begin Source File

SOURCE=.\res\black.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\FiveInRow.ico
# End Source File
# Begin Source File

SOURCE=.\res\FiveInRow.rc2
# End Source File
# Begin Source File

SOURCE=.\res\FiveInRowDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\gezi.bmp
# End Source File
# Begin Source File

SOURCE=.\res\gezimask.bmp
# End Source File
# Begin Source File

SOURCE=.\res\help.bmp
# End Source File
# Begin Source File

SOURCE=.\res\IOCN.ico
# End Source File
# Begin Source File

SOURCE=.\res\lose.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mask.bmp
# End Source File
# Begin Source File

SOURCE=.\res\paopao.bmp
# End Source File
# Begin Source File

SOURCE=.\res\paopao2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\paopaomask.bmp
# End Source File
# Begin Source File

SOURCE=.\res\paopaomask2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\qipan.bmp
# End Source File
# Begin Source File

SOURCE=.\res\surrender.bmp
# End Source File
# Begin Source File

SOURCE=.\res\t1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\welcome.bmp
# End Source File
# Begin Source File

SOURCE=.\res\white.bmp
# End Source File
# Begin Source File

SOURCE=.\res\white.cur
# End Source File
# Begin Source File

SOURCE=.\res\win.bmp
# End Source File
# End Group
# Begin Group "GameUI"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\GameUI\FiveInRow.cpp
# End Source File
# Begin Source File

SOURCE=.\GameUI\FiveInRow.h
# End Source File
# Begin Source File

SOURCE=.\GameUI\MainWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\GameUI\MainWnd.h
# End Source File
# Begin Source File

SOURCE=.\GameUI\Welcome.cpp
# End Source File
# Begin Source File

SOURCE=.\GameUI\Welcome.h
# End Source File
# End Group
# Begin Group "GameLogical"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\GameLogical\Chess.cpp
# End Source File
# Begin Source File

SOURCE=.\GameLogical\Chess.h
# End Source File
# End Group
# Begin Group "GameData"

# PROP Default_Filter ""
# End Group
# Begin Source File

SOURCE=.\res\Btn.wav
# End Source File
# Begin Source File

SOURCE=.\res\lose.wav
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\res\SS103.wav
# End Source File
# Begin Source File

SOURCE=.\res\win.wav
# End Source File
# End Target
# End Project
# Section FiveInRow : {72ADFD78-2C39-11D0-9903-00A0C91BC942}
# 	1:11:IDB_SPLASH1:104
# 	2:21:SplashScreenInsertKey:4.0
# End Section
