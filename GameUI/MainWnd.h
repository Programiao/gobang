#if !defined(AFX_MAINWND_H__C603AB5A_1E00_4F12_8180_FCDA917E776F__INCLUDED_)
#define AFX_MAINWND_H__C603AB5A_1E00_4F12_8180_FCDA917E776F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MainWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMainWnd window
// 
#include "Chess.h"
#include <Mmsystem.h> 
#include <IOSTREAM>
#include <FSTREAM>
#include "Welcome.h"
#include <STDLIB.H>
using namespace std;
#pragma  comment(lib,"Winmm.lib")


class CMainWnd : public CWnd
{
private:

	//黑白棋子.空棋位.最大行列数等的代号
	enum{BLACK = 0, WHITE = 1, EMPTY = -1, MAXROW = 15, OUTSCOPE = -1};

	//游戏状态代号
	enum{STOP = -1, TOPEOPLE = 2, ONBLACK = 0, ONWHITE = 1 ,					//三种游戏模式
		WIN = 3, LOSE = 4, SURRENDER = 5, NORMAL = 6, ABOUTUS = 7, HELP = 8};	//图片相关
	// 
	//触发对话相关 
	enum{TOPEOPLEWIN = 9, TOPEOPLELOSE = 10, ATTACK = 1, DEFFENCE = 0};

	//界面的各种尺寸
	enum{UNITWIDTH = 36, UPWIDTH = 30, WIDTH = 800, HEIGHT = 600,
		PICWIDTH = 457, PICHEIGHT = 318, ABOUTWIDTH = 635, ABOUTHEIGHT = 286,
		HELPWIDTH = 267 , HELPHEIGHT = 387};
	const CRect QIPAN;	//棋盘区域
	const CRect TIME;	//时间显示的区域
	// 

	//记录游戏状态的相关变量
	int m_TextState;		//记录要显示的台词
	int m_State;			//记录游戏状态
	int m_PreState;			//记录游戏结束前的状态,以便在悔棋时恢复
	int m_picture;			//记录要显示的图片
	bool m_video;			//记录是否为播放录像模式
	int m_NumOfBack;		//记录悔棋步数,触发对话使用
	long m_Time1;			//记录玉书的时间
	long m_Time2;			//青石的时间
	bool whichTimer;		//切换计时使用
	CPoint m_Point;			//鼠标当前所在棋盘格
	CPoint m_PrePoint;		//鼠标上次所在的棋盘格
	CPoint m_LastStep;		//最后一步棋
	CPoint m_TiShi;			//记录提示
	bool m_ShowPos;			//记录是否显示落子点对话
	bool m_Num;				//数字标号开关
	bool m_SoundOn;			//音效开关
	// 

	//进行棋局播放时的ifstream
	ifstream m_Fin;
	//棋局信息
	Chess chess;	
		

	//各种资源变量
	// 光标
	HCURSOR m_BlackCursor;	
	HCURSOR m_WhiteCursor;
	

	CDC* m_WinDC;

	//背景
	CBitmap m_Bg;
	CDC m_BgDC;
	CBitmap m_BlankBg;
	CDC m_BlankBgDC;
	
	//游戏图片
	CBitmap m_AboutUs;
	CDC m_AboutUsDC;

	CBitmap m_Help;
	CDC m_HelpDC;

	CBitmap m_WinShow;
	CDC m_WinShowDC;

	CBitmap m_LoseShow;
	CDC m_LoseShowDC;

	CBitmap m_SurrenderShow;
	CDC m_SurrenderShowDC;

	//指示方格
	CDC m_GeziDC;
	CBitmap m_Gezi;

	CBitmap m_GeziMask;
	CDC m_GeziMaskDC;

	
	//对话泡泡
	CDC m_Paopao1DC;
	CBitmap m_Paopao1;
	CDC m_Paopao2DC;
	CBitmap m_Paopao2;

	CBitmap m_PaopaoMask1;
	CDC m_PaopaoMask1DC;
	CBitmap m_PaopaoMask2;
	CDC m_PaopaoMask2DC;
	
	//黑白棋子
	CDC m_BlackDC;
	CBitmap m_Black;
	CDC m_WhiteDC;
	CBitmap m_White;

	CBitmap m_Mask;	
	CDC m_MaskDC;
	
	void initGame(CPaintDC &dc);					//初始化游戏参数
	// 
	//图片显示函数
	void initBitmap(); //初始化位图
	void showText(CPaintDC &dc);					//显示对话
	void showTime(CPaintDC &dc);					//显示时间
	void showNum(CPaintDC &dc);						//显示棋子标号
	void showChess(CPaintDC &dc);					//显示棋子及指示方格
	void showWhite(CPaintDC &dc, CPoint pos);		//显示白棋子
	void showBlack(CPaintDC &dc, CPoint pos);		//显示黑子
	void showGezi(CPaintDC &dc, CPoint pos);		//显示指示方格
	//void showBack(CPaintDC &dc, CPoint pos);		//不用
	void showPaopao(CPaintDC &dc, int num);			//显示对话气泡,1为玉书,0为青石
	void showPicture(CPaintDC &dc, int picture);	//显示游戏图片,胜利,失败,投降等
	CPoint getPosition(CPoint point);				//将鼠标坐标转化成棋盘索引
	CPoint getBitPosition(CPoint pos);				//将棋盘索引转换成贴图左上角坐标
	//

	void next(CPoint pos);							//下一步棋
	void updateMenu();								//更新菜单
	void playBGM(CString name = _T("迷仙引.wav"), bool type = 1);	//播放音乐
	void changeCursor();							//更新光标



// Construction
public:
	CMainWnd();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainWnd)
	//}}AFX_VIRTUAL

// Implementation
public:

	virtual ~CMainWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMainWnd)
	afx_msg void OnPaint();
	afx_msg void OnToPeople();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnNum();
	afx_msg void OnSaveFile();
	afx_msg void OnLoadFile();
	afx_msg void OnBlack();
	afx_msg void OnWhite();
	afx_msg void OnBackStep();
	afx_msg void OnTishi();
	afx_msg void OnSurrender();
	afx_msg void OnExit();
	afx_msg void OnBgm();
	afx_msg void OnSound();
	afx_msg void OnMciNotify(WPARAM wParam,LPARAM lParam);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBgm1();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBgm2();
	afx_msg void OnBgm3();
	afx_msg void OnBgm4();
	afx_msg void OnBgm5();
	afx_msg void OnHelp();
	afx_msg void OnAboutus();
	afx_msg void OnVideo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINWND_H__C603AB5A_1E00_4F12_8180_FCDA917E776F__INCLUDED_)
