// FiveInRow.h : main header file for the FIVEINROW application
//

#if !defined(AFX_FIVEINROW_H__414FC920_535E_4492_B948_89D78CB750D4__INCLUDED_)
#define AFX_FIVEINROW_H__414FC920_535E_4492_B948_89D78CB750D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h" 
#include "MainWnd.h"     
// main symbols

/////////////////////////////////////////////////////////////////////////////
// CFiveInRowApp:
// See FiveInRow.cpp for the implementation of this class
//

class CFiveInRowApp : public CWinApp
{
public:
	CFiveInRowApp();
	CMainWnd* m_MainWnd;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFiveInRowApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CFiveInRowApp)

		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIVEINROW_H__414FC920_535E_4492_B948_89D78CB750D4__INCLUDED_)
