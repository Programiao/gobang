// MainWnd.cpp : implementation file
//

#include "stdafx.h"
#include "FiveInRow.h"
#include "MainWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[]  =  __FILE__; 
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainWnd

CMainWnd::CMainWnd(): QIPAN(  30,  30,  570,  570 ),  TIME(  600,  550,  800,  600  )
{
	//欢迎界面
	CWelcome welcome( this ); 
	welcome.DoModal(); 
}

CMainWnd::~CMainWnd()
{
}


BEGIN_MESSAGE_MAP( CMainWnd,  CWnd )
//{{AFX_MSG_MAP( CMainWnd )
ON_WM_PAINT()
ON_COMMAND( IDM_TOPEOPLE,  OnToPeople )
ON_WM_LBUTTONDOWN()
ON_COMMAND( IDM_NUM,  OnNum )
ON_COMMAND( IDM_SAVEFILE,  OnSaveFile )
ON_COMMAND( IDM_LOADFILE,  OnLoadFile )
ON_COMMAND( IDM_BLACK,  OnBlack )
ON_COMMAND( IDM_WHITE,  OnWhite )
ON_COMMAND( IDM_BACKSTEP,  OnBackStep )
ON_COMMAND( IDM_TISHI,  OnTishi )
ON_COMMAND( IDM_SURRENDER,  OnSurrender )
ON_COMMAND( IDM_EXIT,  OnExit )
ON_COMMAND( IDM_BGM,  OnBgm )
ON_COMMAND( IDM_SOUND,  OnSound )
ON_MESSAGE( MM_MCINOTIFY,  OnMciNotify )
ON_WM_KEYDOWN()
ON_WM_MOUSEMOVE()
ON_COMMAND( IDM_BGM1,  OnBgm1 )
ON_WM_TIMER()
ON_COMMAND( IDM_BGM2,  OnBgm2 )
ON_COMMAND( IDM_BGM3,  OnBgm3 )
ON_COMMAND( IDM_BGM4,  OnBgm4 )
ON_COMMAND( IDM_BGM5,  OnBgm5 )
ON_COMMAND( IDM_HELP,  OnHelp )
ON_COMMAND( IDM_ABOUTUS,  OnAboutus )
ON_COMMAND( IDM_VIDEO,  OnVideo )
//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMainWnd message handlers

void CMainWnd::OnPaint() 
{
	
	CPaintDC pdc( this );  // device context for painting
	CFont myFont; 
	myFont.CreatePointFont( 105,  _T( "微软雅黑" ) ); 
	pdc.SelectObject( &myFont ); 
	pdc.SetBkMode( TRANSPARENT ); 
	
	//第一次OnPaint时初始化
	static bool i = 0; 
	if ( i == 0 )
	{		
		initGame(pdc);		
		i  =  !i;
	}
	//创建内存DC
	CDC dc; 
	CBitmap bitmap; 
	dc.CreateCompatibleDC( &pdc ); 
	bitmap.CreateCompatibleBitmap( &pdc, 800, 600 ); 
	dc.SelectObject( &bitmap ); 
	
	//画背景
	m_BgDC.BitBlt( 0, 0, 800, 600,  &m_BlankBgDC,  0, 0,  SRCCOPY ); 
	dc.BitBlt( 0,  0,  800, 600,  &m_BgDC,  0,  0,  SRCCOPY ); 
	
	//画棋子
	showChess(  ( CPaintDC& ) dc  ); 
	
	//显示对话 
	dc.SetTextColor( RGB( 30, 30, 30 ) ); 
	dc.SelectObject( &myFont ); 
	dc.SetBkMode( TRANSPARENT ); 
	showText( ( CPaintDC& ) dc ); 

	//显示数字标号
	showNum( ( CPaintDC& ) dc ); 
	
	//显示图片
	if( m_picture  !=   NORMAL )
	{
		showPicture( ( CPaintDC& ) dc,  m_picture ); 
		m_State  =  STOP ; 		
	}

	//显示时间
	dc.SetTextColor( RGB( 30, 30, 30 ) ); 	
	if( !m_video )
		showTime( ( CPaintDC& ) dc ); 
	
	
	pdc.BitBlt( 0, 0, 800, 600, &dc, 0, 0, SRCCOPY ); 
}


void CMainWnd::showWhite( CPaintDC &dc,   CPoint pos  )
{
	//背景位图与棋子掩码位图的操作
	m_BgDC.BitBlt( pos.x, pos.y, 36, 36,  &m_MaskDC,  0,  0,  MERGEPAINT ); 
	m_BgDC.BitBlt( pos.x, pos.y, 36, 36,  &m_WhiteDC,  0,  0,  SRCAND ); 
	//将棋盘对应的格子显示出来
	dc.BitBlt( pos.x, pos.y, 36, 36, &m_BgDC,  pos.x, pos.y,  SRCCOPY ); 
}

void CMainWnd::showBlack( CPaintDC &dc,   CPoint pos  )
{
	//背景位图与棋子掩码位图的操作
	m_BgDC.BitBlt( pos.x, pos.y, 36, 36,  &m_MaskDC,  0,  0,  MERGEPAINT ); 
	m_BgDC.BitBlt( pos.x, pos.y, 36, 36,  &m_BlackDC,  0,  0,  SRCAND ); 
	//将棋盘对应的格子显示出来
	dc.BitBlt( pos.x, pos.y, 36, 36, &m_BgDC,  pos.x, pos.y,  SRCCOPY ); 
}

void CMainWnd::showGezi( CPaintDC &dc,   CPoint pos  )
{
	//背景位图与棋子掩码位图的操作
	m_BgDC.BitBlt( pos.x, pos.y, 36, 36,  &m_GeziMaskDC,  0,  0,  MERGEPAINT ); 
	m_BgDC.BitBlt( pos.x, pos.y, 36, 36,  &m_GeziDC,  0,  0,  SRCAND ); 
	//将棋盘对应的格子显示出来
	dc.BitBlt( pos.x, pos.y, 36, 36, &m_BgDC,  pos.x, pos.y,  SRCCOPY ); 
}


void CMainWnd::showPaopao( CPaintDC &dc,   int num  )
{
	if( num == 1 )
	{	
		m_BgDC.BitBlt( 565,  310,  230,  140,  &m_PaopaoMask1DC,  0,  0,  MERGEPAINT ); 
		m_BgDC.BitBlt( 565,  310,  230,  140,  &m_Paopao1DC,  0,  0,  SRCAND ); 
		dc.BitBlt( 565,  310,  230,  140,  &m_BgDC,  565,  310,  SRCCOPY ); 
	}
	if( num == 0 )
	{
		m_BgDC.BitBlt( 565, 310, 230, 140,  &m_PaopaoMask2DC,  0,  0,  MERGEPAINT ); 
		m_BgDC.BitBlt( 565, 310, 230, 140,  &m_Paopao2DC,  0,  0,  SRCAND ); 
		dc.BitBlt( 565, 310, 230, 140, &m_BgDC,  565,  310,  SRCCOPY ); 
	}
}


CPoint CMainWnd::getPosition(  CPoint point  )
{
	CPoint pos; 
	if (  point.x+16 < 30 || point.y+16 < 30 || point.x+16 >= 570 || point.y+16 >= 570 )
	{
		pos.x = OUTSCOPE; 
		pos.y = OUTSCOPE; 
	}
	else
	{
		pos.x = ( point.x+16-30 )/36; 
		pos.y = ( point.y+16-30 )/36; 
	}
	return pos; 
}

CPoint CMainWnd::getBitPosition(  CPoint pos  )
{
	pos.x  =  pos.x * UNITWIDTH + UPWIDTH; 
	pos.y  =  pos.y * UNITWIDTH + UPWIDTH; 
	return pos; 
}

void CMainWnd::next(  CPoint pos  )
{
	CPoint bitPoint  =  getBitPosition( pos ); 
	
	if ( m_SoundOn )
	{
		PlaySound( MAKEINTRESOURCE( IDW_NEXT ),  AfxGetResourceHandle(),  
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT|SND_NOSTOP ); 
	}
	m_LastStep = pos; 
	//记录该步棋
	chess.nextStep( pos ); 
	if( whichTimer )
	{
		KillTimer(  1  ); 
		SetTimer(  2 ,  1000 ,  NULL  ); 
	}
	else
	{
		KillTimer(  2  ); 
		SetTimer(  1 ,  1000 ,  NULL  ); 
	}
	whichTimer  =  !whichTimer; 
	
	//判断输赢
	if( chess.ifWin() )
	{
		
		switch( m_State )
		{
		case TOPEOPLE:
			if( !whichTimer )
			{
				m_TextState  =  600; 
				SetTimer( 600, 2000, NULL ); 
				m_ShowPos  =  false; 
			}
			else
			{
				m_TextState  =  610; 
				SetTimer( 610, 2000, NULL ); 
				m_ShowPos  =  false; 
			}
			
			break; 
		case ONBLACK:
			if( chess.getColor()  ==  WHITE )
			{
				m_picture  =  WIN; 
			}
			else
			{
				m_picture  =  LOSE; 
			}
			break; 
		case ONWHITE:
			if( chess.getColor()  ==  BLACK )
			{
				m_picture  =  WIN; 
			}
			else
			{
				m_picture  =  LOSE; 
			}
			break; 
		default:
			break; 		
		}
		KillTimer( 1 ); 
		KillTimer( 2 ); 
		chess.end(); 
		
	}
	else
	{
		if( chess.getStepNum() > 224 )
		{
			MessageBox( "棋盘下满了, 你们真是不相上下啊" ); 
			chess.end(); 
		}
	}
	
	changeCursor(); 
	Invalidate(); 
	
}

void CMainWnd::changeCursor()
{
	if( !chess.ifBegin() )
	{
		SetClassLong( m_hWnd, GCL_HCURSOR, ( LONG )IDC_ARROW ); 
		return; 
	}
	if( chess.getColor() )
	{
		SetClassLong( m_hWnd, GCL_HCURSOR, ( LONG )m_WhiteCursor ); 
	}
	else
	{
		SetClassLong( m_hWnd, GCL_HCURSOR, ( LONG )m_BlackCursor ); 
	}
}

void CMainWnd::playBGM(  CString name ,  bool type  )
{
	
	MCI_OPEN_PARMS   OpenParms;  
	//MCI_DEVTYPE_SEQUENCER; 此为midi格式, MCI_DEVTYPE_WAVEFORM_AUDIO; 是wav格式; 
	//OpenParms.lpstrDeviceType    =   ( LPCSTR ) MCI_DEVTYPE_SEQUENCER; 
	if( type )
	{
		OpenParms.lpstrDeviceType    =   ( LPCSTR ) MCI_DEVTYPE_WAVEFORM_AUDIO; 
	}
	else
	{
		OpenParms.lpstrDeviceType    =   ( LPCSTR ) MCI_DEVTYPE_SEQUENCER; 
	}
	OpenParms.lpstrElementName    =    name ;  
	OpenParms.wDeviceID    =  1;    //打开的设备的标识，后面需要使用 
	mciSendCommand   ( NULL,    MCI_OPEN,    MCI_WAIT  |   MCI_OPEN_TYPE   |   
		MCI_OPEN_TYPE_ID  |   MCI_OPEN_ELEMENT,  ( DWORD )( LPVOID )   &OpenParms );    //打开设备
		
	MCI_PLAY_PARMS   PlayParms;  
	PlayParms.dwCallback  =  ( DWORD )m_hWnd; 
	PlayParms.dwFrom    =    0;    //从什么时间位置播放，单位为毫秒 
	mciSendCommand   ( 1,    MCI_PLAY,  MCI_FROM | MCI_NOTIFY,  ( DWORD )( LPVOID )&PlayParms ); 
	
	
}

void CMainWnd::OnToPeople() 
{
	KillTimer( 1 ); 
	KillTimer( 2 ); 
	m_Time1  =  0; 
	m_Time2  =  0; 
	whichTimer  =  1; 
	m_ShowPos  =  true; 
	SetTimer(  1,  1000 ,  NULL  ); 
	
	chess = Chess(); 
	chess.begin(); 
	m_NumOfBack  =  0; 
	m_LastStep = chess.getLast(); 
	updateMenu(); 
	Invalidate(); 
	SetClassLong( m_hWnd, GCL_HCURSOR, ( LONG )m_BlackCursor ); 
	m_PreState  =  TOPEOPLE; 
	m_State = TOPEOPLE; 
	m_picture  =  NORMAL; 
	m_TiShi = -1;
}

void CMainWnd::OnLButtonDown( UINT nFlags,  CPoint point ) 
{
	if( m_picture  !=   NORMAL )
	{
		m_picture  =  NORMAL; 
		Invalidate(); 
		return; 
	}
	
	CPoint pos = getPosition( point ); 
	
	if ( chess.ifNext( pos ) )
	{
		if( m_LastStep.x  ==  -1 && ( pos.x !=  7 || pos.y !=  7 ) )
		{
			m_TextState  =  50; 
			m_ShowPos  =  0; 
		}
		else
		{
			if( chess.getChessList().empty() )
			{
				m_TextState  =  0; 
				m_ShowPos  =  1; 
			}
			//对话触发
			//玩家落子和提示相同
			CPoint tishi; 
			chess.getNextStep( chess.getColor(), tishi ); 
			if( pos  ==  tishi && m_ShowPos  ==  true )
			{
				srand( time( NULL ) ); 
				int r  =  rand() % 10; 
				if( r  ==  0 && chess.getStepNum() > 2)
				{
					m_TextState  =  300; 
					SetTimer( m_TextState,  2000,  NULL ); 
					m_ShowPos  =  false; 

				}
				
			}
			if( chess.getStepNum() > 200 && m_ShowPos  ==  true )
			{
				srand( time( NULL ) ); 
				int r  =  rand() % 10; 
				if( r  ==  0 && chess.getStepNum() > 2)
				{
					m_TextState  =  400; 
					SetTimer( m_TextState,  2000,  NULL ); 
					m_ShowPos  =  false; 				
				}
			}
			//
			
			next( pos ); 

			if( chess.ifBegin() )
			{
				if( m_State == ONBLACK )
				{
					chess.getNextStep( WHITE, pos ); 
					next( pos ); 
				}
				if( m_State == ONWHITE )
				{
					chess.getNextStep( BLACK, pos ); 
					next( pos ); 
				}
				//对话触发
				// 
				// 
				if( m_State  !=   TOPEOPLE && m_ShowPos  ==  true )
				{
					if( chess.attOrDef()  ==  DEFFENCE )
					{
						srand( time( NULL ) ); 
						int r  =  rand() % 5; 
						if(  r  ==  0  && chess.getStepNum() > 2)
						{
							m_TextState  =  (  rand() % 4  ) * 10 + 100; 
							SetTimer( m_TextState,  2000,  NULL ); 
							m_ShowPos  =  false; 
						}
						
					}
					else
					{
						srand( time( NULL ) ); 
						int r  =  rand() % 6; 
						if(  r  ==  0  && chess.getStepNum() > 2)
						{
							m_TextState  =  (  rand() % 2  ) * 10 + 200; 
							SetTimer( m_TextState,  2000,  NULL ); 
							m_ShowPos  =  false; 
						}
						
					}
					
				}
					
				////////////
			}
			
			
			//提示消除		
			m_TiShi.x = -1; 
			//
			
			
			updateMenu(); 
		}
		Invalidate(); 
	}
	CWnd::OnLButtonDown( nFlags,  point ); 
}

void CMainWnd::OnNum() 
{	
	if ( GetMenu()->GetSubMenu( 2 )->GetMenuState( IDM_NUM,  MF_BYCOMMAND ) == MF_CHECKED )
	{
		GetMenu()->GetSubMenu( 2 )->CheckMenuItem( IDM_NUM,  MF_BYCOMMAND | MF_UNCHECKED  ); 
		m_Num = 0; 
	}
	else
	{
		GetMenu()->GetSubMenu( 2 )->CheckMenuItem( IDM_NUM,  MF_BYCOMMAND | MF_CHECKED  ); 
		m_Num = 1; 
	}
	Invalidate(); 
}

void CMainWnd::OnSaveFile() 
{
	if( chess.getChessList().empty() )
		return; 
	
	CFileDialog fd( false, "fs", NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		"保存文件( *.fs )|*.fs||", this ); 
	if(  fd.DoModal()  == IDCANCEL  )
		return; 
	
	CString name(  fd.GetPathName()  ); 
	
	ofstream fout( name ); 
	fout << m_State; 
	list <CPoint>::iterator theIterator; 
	list <CPoint> chesslist = chess.getChessList(); 
	for(  theIterator  =  chesslist.begin();  theIterator  !=   chesslist.end();  theIterator++  )
	{
		fout << ' ' << ( *theIterator ).x << ' ' << ( *theIterator ).y; 
	}
	fout.clear(); 
	fout.close(); 
	
}

void CMainWnd::OnLoadFile() 
{
	
	CFileDialog fd( true, "fs", NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		"保存文件( *.fs )|*.fs||", this ); 
	if(  fd.DoModal()  == IDCANCEL  )
		return; 
	
	CString name(  fd.GetPathName()  ); 
	ifstream fin( name ); 
	fin>>m_State; 
	
	chess = Chess(); 
	chess.begin(); 
	
	CPoint pos; 
	while( fin>>pos.x &&	fin>>pos.y )
	{
		chess.nextStep( pos ); 
	}
	fin.clear(); 
	fin.close(); 
	if( chess.ifWin() )
		chess.end(); 
	m_LastStep = chess.getLast(); 

	updateMenu(); 
	
	changeCursor(); 
	
	Invalidate(); 
}

void CMainWnd::OnBlack() 
{
	KillTimer( 1 ); 
	KillTimer( 2 ); 
	m_Time1  =  0; 
	m_Time2  =  0; 
	whichTimer  =  1; 
	SetTimer(  1,  1000 ,  NULL  ); 
	
	chess = Chess(); 
	m_NumOfBack  =  0; 
	m_ShowPos  =  true; 
	chess.begin(); 
	m_LastStep = chess.getLast(); 
	updateMenu(); 
	changeCursor(); 
	m_PreState  =  ONBLACK; 
	m_State = ONBLACK; 
	m_picture  =  NORMAL; 
	m_TiShi = -1;
	Invalidate(); 
	
}

void CMainWnd::OnWhite() 
{
	KillTimer( 1 ); 
	KillTimer( 2 ); 
	m_Time1  =  0; 
	m_Time2  =  0; 
	whichTimer  =  0; 
	SetTimer(  2,  1000 ,  NULL  ); 
	
	chess = Chess(); 
	m_NumOfBack  =  0; 
	m_ShowPos  =  true; 
	chess.begin(); 
	next( CPoint( 7, 7 ) ); 
	m_LastStep = chess.getLast(); 
	updateMenu(); 
	changeCursor(); 
	m_TiShi = -1;
	m_PreState  =  ONWHITE; 
	m_State = ONWHITE; 
	m_picture  =  NORMAL; 
	Invalidate(); 
	
}

void CMainWnd::OnBackStep() 
{
	if( m_picture  !=   NORMAL )
	{
		return; 
	}
	if( !chess.getChessList().empty() )
	{
		if( whichTimer )
		{
			KillTimer(  1  ); 
			SetTimer(  2 ,  1000 ,  NULL  ); 
		}
		else
		{
			KillTimer(  2  ); 
			SetTimer(  1 ,  1000 ,  NULL  ); 
		}
		whichTimer  =  !whichTimer; 
		m_State  =  m_PreState; 
		chess.backStep(); 
		m_TiShi.x = -1;

		m_NumOfBack++; 
		if( m_NumOfBack > 3 && m_State  !=   TOPEOPLE )
		{
			m_TextState  =  1000; 
			m_ShowPos  =  false; 
		}


		changeCursor(); 
		if( m_State !=  TOPEOPLE )
		{
			while( (int)chess.getColor() != m_State )
			{

				m_LastStep = chess.getLast(); 
				chess.backStep(); 
				changeCursor(); 
			
			}
		}
		if ( m_SoundOn )
		{
			PlaySound( MAKEINTRESOURCE( IDW_BACK ), AfxGetResourceHandle(),  
				SND_ASYNC|SND_RESOURCE|SND_NODEFAULT|SND_NOSTOP ); 
		}
		m_LastStep = chess.getLast(); 
		updateMenu(); 
		Invalidate(); 
	}	
	
}

void CMainWnd::OnTishi() 
{
	if( !chess.getChessList().empty() && chess.ifBegin() )
	{
		//if( m_TiShi.x == -1 )
		{
			CPoint pos; 
			chess.getNextStep( chess.getColor(), pos );
			//chess.getTreeStep( chess.getColor(), pos ,0); 
			m_TiShi  =  pos; 
			updateMenu(); 
			Invalidate(); 
		}
	}
	
}

void CMainWnd::OnSurrender() 
{
	KillTimer( 1 ); 
	KillTimer( 2 ); 
	m_picture  =  SURRENDER; 
	chess.end(); 
	changeCursor(); 
	updateMenu(); 
	Invalidate(); 
}

void CMainWnd::OnExit() 
{
	mciSendCommand   ( 1,    MCI_CLOSE,    MCI_WAIT,    NULL ); 
	DestroyWindow(); 
	
}

void CMainWnd::OnBgm() 
{
	if ( GetMenu()->GetSubMenu( 1 )->GetMenuState(  IDM_BGM,  MF_BYCOMMAND ) == MF_CHECKED )
	{
		mciSendCommand   ( 1,    MCI_STOP,    NULL,    NULL ); 
		GetMenu()->GetSubMenu( 1 )->CheckMenuItem(  IDM_BGM,  MF_BYCOMMAND | MF_UNCHECKED  ); 
	}
	else
	{
		mciSendCommand   ( 1,    MCI_PLAY,  NULL, NULL ); 
		GetMenu()->GetSubMenu( 1 )->CheckMenuItem(  IDM_BGM,  MF_BYCOMMAND | MF_CHECKED  ); 
	}
	
}

void CMainWnd::OnSound() 
{
	if( m_SoundOn )
	{
		GetMenu()->GetSubMenu( 1 )->CheckMenuItem(  IDM_SOUND,  MF_BYCOMMAND | MF_UNCHECKED  ); 
		m_SoundOn = 0; 
	}
	else
	{
		GetMenu()->GetSubMenu( 1 )->CheckMenuItem(  IDM_SOUND,  MF_BYCOMMAND | MF_CHECKED  ); 
		m_SoundOn = 1; 
	}
	
}

void CMainWnd::OnKeyDown( UINT nChar,  UINT nRepCnt,  UINT nFlags ) 
{
	if ( nChar == 66 )
	{
		OnBackStep(); 
	}
	if ( nChar == 84 )
	{
		OnTishi(); 
	}
	if ( nChar == 65 )
	{
		OnToPeople(); 
	}
	if ( nChar == 83 )
	{
		OnBlack(); 
	}
	if ( nChar == 68 )
	{
		OnWhite(); 
	}
	
	CWnd::OnKeyDown( nChar,  nRepCnt,  nFlags ); 
}

void CMainWnd::OnMciNotify( WPARAM wParam, LPARAM lParam ) //背景音乐循环播放
{
    switch ( wParam )
    {
    case MCI_NOTIFY_SUCCESSFUL:
        {
			MCI_PLAY_PARMS PlayParms; 
            PlayParms.dwFrom  =  30; 
            PlayParms.dwCallback  =  ( DWORD )m_hWnd; 
            mciSendCommand( 1,  MCI_SEEK,  MCI_SEEK_TO_START,  NULL ); 
            mciSendCommand( 1,  MCI_PLAY,  MCI_NOTIFY,  ( DWORD )( LPVOID )&PlayParms ); 
        }
        break;  
	default:
        break; 
    }
}

void CMainWnd::OnMouseMove( UINT nFlags,  CPoint point ) 
{
	if( m_picture  !=   NORMAL )
	{
		return; 
	}
	
	if( chess.ifBegin() )
	{	
		m_Point  =  getPosition( point ); 
		CPoint bitPoint  =  getBitPosition( m_Point ); 
		CPoint bitPointPre  =  getBitPosition( m_PrePoint ); 
		
		if ( m_Point !=  m_PrePoint )
		{
			if( m_Point.x  !=   OUTSCOPE && m_Point.y  !=   OUTSCOPE )
			{
				CClientDC dc( this ); 
				m_BgDC.BitBlt(  bitPoint.x,  bitPoint.y,  36,  36,  
					&m_GeziMaskDC,  0,  0,  MERGEPAINT ); 
				m_BgDC.BitBlt(  bitPoint.x,  bitPoint.y,  36,  36,  
					&m_GeziDC,  0,  0,  SRCAND ); 
				dc.BitBlt( bitPoint.x,  bitPoint.y,  36,  36,  
					&m_BgDC,  bitPoint.x,  bitPoint.y,  SRCCOPY ); 
				//InvalidateRect( CRect( m_Point.x,  m_Point.y,  m_Point.x+36,  m_Point.y + 36 ) ); 
			}
			if( m_PrePoint.x  !=   OUTSCOPE && m_PrePoint.y  !=   OUTSCOPE )
			{
				InvalidateRect( CRect( bitPointPre.x,  bitPointPre.y,  
					bitPointPre.x+36,  bitPointPre.y + 36 ) ); 
			}
			m_PrePoint  =  m_Point; 
		}
		
		
	}
	CWnd::OnMouseMove( nFlags,  point ); 
}

void CMainWnd::updateMenu()
{
	if ( chess.getChessList().empty() )
	{
		GetMenu()->GetSubMenu( 0 )->EnableMenuItem( 
			IDM_BACKSTEP,  MF_BYCOMMAND | MF_DISABLED | MF_GRAYED ); 		
	}
	else
	{
		GetMenu()->GetSubMenu( 0 )->EnableMenuItem( 
			IDM_BACKSTEP,  MF_BYCOMMAND | MF_ENABLED  ); 		
	}
	if ( !chess.ifBegin() )
	{
		GetMenu()->GetSubMenu( 0 )->EnableMenuItem( 
			IDM_TISHI,  MF_BYCOMMAND | MF_DISABLED | MF_GRAYED ); 
		GetMenu()->GetSubMenu( 0 )->EnableMenuItem( 
			IDM_SURRENDER,  MF_BYCOMMAND | MF_DISABLED | MF_GRAYED ); 
	}
	else
	{
		GetMenu()->GetSubMenu( 0 )->EnableMenuItem( 
			IDM_TISHI,  MF_BYCOMMAND | MF_ENABLED ); 	
		GetMenu()->GetSubMenu( 0 )->EnableMenuItem( 
			IDM_SURRENDER,  MF_BYCOMMAND | MF_ENABLED  ); 
	}
}



void CMainWnd::OnTimer( UINT nIDEvent ) 
{
	switch(  nIDEvent  )
	{
	case 1:
		m_Time1++; 
		break; 
	case 2:
		m_Time2++; 
		break; 
	case 3:
		{
			
			CPoint pos; 
			
			m_Fin>>pos.x ; 
			m_Fin>>pos.y; 
			if( !m_Fin )
			{
				m_video  =  false; 
				//chess.end(); 
				m_Time2  =  0; 
				m_Time1  =  0; 
				KillTimer( 3 ); 
				KillTimer( 1 ); 
				KillTimer( 2 ); 
				m_Fin.close(); 
				Invalidate(); 
				return; 
			}
			next( pos ); 
			m_LastStep = pos; 
			updateMenu(); 
			break; 
			
		}
	case 100:
		if(  m_TextState  <  101  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			m_ShowPos  =  true; 
			KillTimer( 100 ); 
		}
		break; 

	case 110:
		if(  m_TextState  <  111  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			m_ShowPos  =  true; 
			KillTimer( 110 ); 
		}
		break; 
	case 120:
		if(  m_TextState  <  121  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			m_ShowPos  =  true; 
			KillTimer( 120 ); 
		}
		break; 
	case 130:
		if(  m_TextState  <  133  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			m_ShowPos  =  true; 
			KillTimer( 130 ); 
		}
		break; 
	case 200:
		if(  m_TextState  <  201  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			m_ShowPos  =  true; 
			KillTimer( 200 ); 
		}
		break; 
	case 210:
		if(  m_TextState  <  211  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			m_ShowPos  =  true; 
			KillTimer( 210 ); 
		}
		break; 
	case 300:
		if(  m_TextState  <  301  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			m_ShowPos  =  true; 
			KillTimer( 300 ); 
		}
		break; 
	case 400:
		if(  m_TextState  <  401  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			m_ShowPos  =  true; 
			KillTimer( 400 ); 
		}
		break; 
	case 600:
		if(  m_TextState  <  601  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			KillTimer( 600 ); 
		}
		break; 
	case 610:
		if(  m_TextState  <  611  )
		{	
			m_TextState++; 
		}
		else
		{
			m_TextState  =  0; 
			KillTimer( 610 ); 
		}
		break; 

	default:
		break; 
	}
	Invalidate(); 
	CWnd::OnTimer( nIDEvent ); 
}

void CMainWnd::OnBgm1() 
{
	for( int i = 0; i < 5; i++ )
	{
		GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
			CheckMenuItem(  i ,  MF_BYPOSITION | MF_UNCHECKED  ); 
	}	
	mciSendCommand   ( 1,    MCI_CLOSE,    NULL,    NULL ); 
	playBGM(  _T( "迷仙引.WAV" )  ); 
	
	GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
		CheckMenuItem(  0 ,  MF_BYPOSITION | MF_CHECKED  ); 
	
	if ( GetMenu()->GetSubMenu( 1 )->GetMenuState(  IDM_BGM,  MF_BYCOMMAND ) == MF_UNCHECKED )
	{
		GetMenu()->GetSubMenu( 1 )->CheckMenuItem(  IDM_BGM,  MF_BYCOMMAND | MF_CHECKED  ); 
	}
	
}

void CMainWnd::OnBgm2() 
{
	for( int i = 0; i < 5; i++ )
	{
		GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
			CheckMenuItem(  i ,  MF_BYPOSITION | MF_UNCHECKED  ); 
	}
	mciSendCommand   ( 1,    MCI_CLOSE,    NULL,    NULL ); 
	playBGM(  _T( "御剑江湖.WAV" )  ); 
	
	GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
		CheckMenuItem(  1 ,  MF_BYPOSITION | MF_CHECKED  ); 
	
	if ( GetMenu()->GetSubMenu( 1 )->GetMenuState(  IDM_BGM,  MF_BYCOMMAND ) == MF_UNCHECKED )
	{
		GetMenu()->GetSubMenu( 1 )->CheckMenuItem(  IDM_BGM,  MF_BYCOMMAND | MF_CHECKED  ); 
	}
	
}

void CMainWnd::OnBgm3() 
{
	for( int i = 0; i < 5; i++ )
	{
		GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
			CheckMenuItem(  i ,  MF_BYPOSITION | MF_UNCHECKED  ); 
	}
	mciSendCommand   ( 1,    MCI_CLOSE,    NULL,    NULL ); 
	playBGM(  _T( "蝶恋.wav" ) ); 
	
	GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
		CheckMenuItem(  2 ,  MF_BYPOSITION | MF_CHECKED  ); 
	
	if ( GetMenu()->GetSubMenu( 1 )->GetMenuState(  IDM_BGM,  MF_BYCOMMAND ) == MF_UNCHECKED )
	{
		GetMenu()->GetSubMenu( 1 )->CheckMenuItem(  IDM_BGM,  MF_BYCOMMAND | MF_CHECKED  ); 
	}
}

void CMainWnd::OnBgm4() 
{
	for( int i = 0; i < 5; i++ )
	{
		GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
			CheckMenuItem(  i ,  MF_BYPOSITION | MF_UNCHECKED  ); 
	}
	mciSendCommand   ( 1,    MCI_CLOSE,    NULL,    NULL ); 
	playBGM(  _T( "玉满堂.WAV" )  ); 
	
	GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
		CheckMenuItem(  3 ,  MF_BYPOSITION | MF_CHECKED  ); 
	
	if ( GetMenu()->GetSubMenu( 1 )->GetMenuState(  IDM_BGM,  MF_BYCOMMAND ) == MF_UNCHECKED )
	{
		GetMenu()->GetSubMenu( 1 )->CheckMenuItem(  IDM_BGM,  MF_BYCOMMAND | MF_CHECKED  ); 
	}
	
}

void CMainWnd::OnBgm5() 
{
	for( int i = 0; i < 5; i++ )
	{
		GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
			CheckMenuItem(  i ,  MF_BYPOSITION | MF_UNCHECKED  ); 
	}
	mciSendCommand   ( 1,    MCI_CLOSE,    NULL,    NULL ); 
	playBGM(  _T( "仙竹林.WAV" )  ); 
	
	GetMenu()->GetSubMenu( 1 )->GetSubMenu( 2 )->
		CheckMenuItem(  4 ,  MF_BYPOSITION | MF_CHECKED  ); 
	
	if ( GetMenu()->GetSubMenu( 1 )->GetMenuState(  IDM_BGM,  MF_BYCOMMAND ) == MF_UNCHECKED )
	{
		GetMenu()->GetSubMenu( 1 )->CheckMenuItem(  IDM_BGM,  MF_BYCOMMAND | MF_CHECKED  ); 
	}
	
}

void CMainWnd::showPicture(  CPaintDC &dc,  int picture  )
{
	int left  =  WIDTH / 2 - PICWIDTH / 2; 
	int right  =  HEIGHT / 2 - PICHEIGHT / 2; 
	switch( picture )
	{
	case WIN:
		dc.BitBlt( left,  right,  PICWIDTH,  PICWIDTH,  &m_WinShowDC,  0,  0,  SRCCOPY ); 
		break; 
	case LOSE:
		dc.BitBlt( left,  right,  PICWIDTH,  PICWIDTH,  &m_LoseShowDC,  0,  0,  SRCCOPY ); 
		break; 
	case SURRENDER:
		dc.BitBlt( left,  right,  PICWIDTH,  PICWIDTH,  &m_SurrenderShowDC,  0,  0,  SRCCOPY ); 
		break; 
	case ABOUTUS:
		dc.BitBlt( WIDTH / 2 - ABOUTWIDTH / 2,  HEIGHT / 2 - ABOUTHEIGHT / 2, 
			ABOUTWIDTH,  ABOUTHEIGHT,  &m_AboutUsDC,  0,  0,  SRCCOPY ); 
		break; 
	case HELP:
		dc.BitBlt( WIDTH / 2 - HELPWIDTH / 2,  HEIGHT / 2 - HELPHEIGHT / 2, 
			HELPWIDTH,  HELPHEIGHT,  &m_HelpDC,  0,  0,  SRCCOPY ); 
		break; 
	default:
		break; 
	}
	
}

void CMainWnd::OnHelp() 
{
	m_picture  =  HELP; 
	Invalidate(); 
	
}

void CMainWnd::OnAboutus() 
{
	m_picture  =  ABOUTUS; 
	Invalidate(); 
	
}

void CMainWnd::initBitmap()
{
	//关于我们
	m_AboutUs.LoadBitmap( IDB_ABOUTUS ); 
	m_AboutUsDC.CreateCompatibleDC( m_WinDC ); 
	m_AboutUsDC.SelectObject( &m_AboutUs );
	
	//帮助
	m_Help.LoadBitmap( IDB_HELP ); 
	m_HelpDC.CreateCompatibleDC( m_WinDC ); 
	m_HelpDC.SelectObject( &m_Help ); 
	
	//初始化背景
	m_Bg.LoadBitmap( IDB_BG ); 
	m_BgDC.CreateCompatibleDC( m_WinDC ); 
	m_BgDC.SelectObject( &m_Bg ); 

	m_BlankBg.LoadBitmap( IDB_BG ); 
	m_BlankBgDC.CreateCompatibleDC( m_WinDC ); 
	m_BlankBgDC.SelectObject( &m_BlankBg ); 
	
	//游戏
	m_WinShow.LoadBitmap( IDB_WIN ); 
	m_WinShowDC.CreateCompatibleDC( m_WinDC ); 
	m_WinShowDC.SelectObject( &m_WinShow ); 
	
	m_LoseShow.LoadBitmap( IDB_LOSE ); 
	m_LoseShowDC.CreateCompatibleDC( m_WinDC ); 
	m_LoseShowDC.SelectObject( &m_LoseShow ); 
	
	m_SurrenderShow.LoadBitmap( IDB_SURRENDER ); 
	m_SurrenderShowDC.CreateCompatibleDC( m_WinDC ); 
	m_SurrenderShowDC.SelectObject( &m_SurrenderShow ); 
	
	//初始化小方格
	m_GeziDC.CreateCompatibleDC( m_WinDC ); 
	m_Gezi.LoadBitmap( IDB_GZ ); 
	m_GeziDC.SelectObject( &m_Gezi ); 
	
	//黑棋子
	m_BlackDC.CreateCompatibleDC( m_WinDC ); 
	m_Black.LoadBitmap( IDB_BLACK ); 
	m_BlackDC.SelectObject( &m_Black ); 
	
	//白棋子
	m_WhiteDC.CreateCompatibleDC( m_WinDC ); 
	m_White.LoadBitmap( IDB_WHITE ); 
	m_WhiteDC.SelectObject( &m_White ); 
	
	//对话泡泡
	m_Paopao1DC.CreateCompatibleDC( m_WinDC ); 
	m_Paopao1.LoadBitmap( IDB_PAO1 ); 
	m_Paopao1DC.SelectObject( &m_Paopao1 ); 
	
	m_Paopao2DC.CreateCompatibleDC( m_WinDC ); 
	m_Paopao2.LoadBitmap( IDB_PAO2 ); 
	m_Paopao2DC.SelectObject( &m_Paopao2 ); 
	
	//棋子掩码图片
	m_MaskDC.CreateCompatibleDC( m_WinDC ); 
	m_Mask.LoadBitmap( IDB_MASK ); 
	m_MaskDC.SelectObject( &m_Mask ); 
	
	//小方格掩码
	m_GeziMaskDC.CreateCompatibleDC( m_WinDC ); 
	m_GeziMask.LoadBitmap( IDB_GMASK ); 
	m_GeziMaskDC.SelectObject( &m_GeziMask ); 
	
	//对话泡泡掩码
	m_PaopaoMask1DC.CreateCompatibleDC( m_WinDC ); 
	m_PaopaoMask1.LoadBitmap( IDB_PAOMASK1 ); 
	m_PaopaoMask1DC.SelectObject( &m_PaopaoMask1 ); 
	
	m_PaopaoMask2DC.CreateCompatibleDC( m_WinDC ); 
	m_PaopaoMask2.LoadBitmap( IDB_PAOMASK2 ); 
	m_PaopaoMask2DC.SelectObject( &m_PaopaoMask2 ); 
	
	//原位图与掩码图片的处理
	m_BlackDC.BitBlt( 0,  0,  36,  36,  &m_MaskDC,  0,  0,  SRCPAINT ); 
	m_WhiteDC.BitBlt( 0,  0,  36,  36,  &m_MaskDC,  0,  0,  SRCPAINT ); 
	m_GeziDC.BitBlt( 0, 0, 36, 36, &m_GeziMaskDC, 0, 0, SRCPAINT ); 
	
	m_Paopao1DC.BitBlt( 0, 0, 230, 140, &m_PaopaoMask1DC, 0, 0, SRCPAINT ); 
	m_Paopao2DC.BitBlt( 0, 0, 230, 140, &m_PaopaoMask2DC, 0, 0, SRCPAINT ); 
	
	//加载光标
	m_BlackCursor = LoadCursor( AfxGetInstanceHandle(), MAKEINTRESOURCE( IDC_BLACK ) ); 
	m_WhiteCursor = LoadCursor( AfxGetInstanceHandle(), MAKEINTRESOURCE( IDC_WHITE ) ); 
}

void CMainWnd::showNum(  CPaintDC &dc  )
{
	//显示数字
	bool color = BLACK; 
	list < CPoint>::iterator theIterator; 
	list < CPoint> chesslist = chess.getChessList(); 
	int n = 1; 
	if( 	m_Num  )
	{
		for(  theIterator  =  chesslist.begin();  theIterator  !=   chesslist.end();  theIterator++  )
		{
			CString str; 
			str.Format( "%d", n ); 
			if ( color == WHITE )
			{		
				dc.SetTextColor( RGB( 0, 0, 0 ) ); 	
			}
			else
			{
				dc.SetTextColor( RGB( 255, 255, 255 ) ); 
			}
			color = !color; 	
			
			if( n < 10 )
			{
				dc.TextOut( getBitPosition( *theIterator ).x+12, 
					getBitPosition( *theIterator ).y+6, str ); 
			}
			else
			{
				dc.TextOut( getBitPosition( *theIterator ).x+7, 
					getBitPosition( *theIterator ).y+6, str ); 
			}
			
			n++; 
		}
		
	}
}

void CMainWnd::showChess(  CPaintDC &dc  )
{
	bool color = BLACK; 
	list < CPoint>::iterator theIterator; 
	list < CPoint> chesslist = chess.getChessList(); 
	for(  theIterator  =  chesslist.begin();  theIterator  !=   chesslist.end();  theIterator++  )
	{
		if ( color == WHITE )
		{
			showWhite( ( CPaintDC& ) dc,  getBitPosition( *theIterator ) ); 			
		}
		else
		{
			showBlack( ( CPaintDC& ) dc,  getBitPosition( *theIterator ) ); 
		}
		color = !color; 	
	}
	m_LastStep = chess.getLast(); 
	//显示最后一步棋
	if (  m_LastStep.x  !=   OUTSCOPE  )
	{
		showGezi( ( CPaintDC& ) dc,  getBitPosition( m_LastStep ) ); 
	}
	//鼠标位置的格子
	if (  m_Point.x  !=   OUTSCOPE && m_Point.y  !=   OUTSCOPE && chess.ifBegin() )
	{
		showGezi( ( CPaintDC& ) dc,  getBitPosition( m_Point ) ); 
	}
	//显示提示方格
	if (  m_TiShi.x  !=   OUTSCOPE  )
	{
		showGezi( ( CPaintDC& ) dc,  getBitPosition( m_TiShi ) ); 
	}
}

void CMainWnd::showTime(  CPaintDC &dc  )
{
	CString time; 
	time.Format( "%d s",  m_Time1 ); 
	dc.TextOut( 610,  570,  time ); 
	
	time.Format( "%d s",  m_Time2 ); 
	dc.TextOut( 730,  570,  time ); 
}


void CMainWnd::showText( CPaintDC &dc )
{
	
	if( m_State  ==  STOP )
	{
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 607, 337, _T("阿书, 若是有空, ") ); 
		dc.TextOut( 607, 371, _T("我们手谈一局吧") ); 
		return; 
	}
	if( chess.getStepNum() == 0 && chess.ifBegin() && m_TextState  ==  0 )
	{
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 610, 350, _T("这次一定赢你!") ); 
		return; 
	}

	//触发的对话
	switch( m_TextState )
	{
	case 50:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 599, 350, _T("第一子要下在正中间") ); 
		break; 
	case 100:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 615, 350, _T("进攻!") ); 
		break; 
	case 101:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 615, 350, _T("不怕你") ); 
		break; 
	case 110:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 605, 350, _T("阿书, 杀气很重啊") ); 
		break; 
	case 111:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 605, 350, _T("......彼此彼此") ); 
		break; 
	case 120:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 600, 350, _T("哈哈, 你快不行咯!") ); 
		break; 
	case 121:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 607, 337, _T("我才使出七分力") ); 
		dc.TextOut( 607, 371, _T("休要张狂") ); 
		break; 
	case 130:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 600, 350, _T("看我一子定乾坤!") ); 
		break; 
	case 131:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 600, 337, _T("别吵") ); 
		dc.TextOut( 600, 371, _T("观棋不语真君子") ); 
		break; 
	case 132:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 607, 337, _T("我不是在观") ); 
		dc.TextOut( 607, 371, _T("是在下......") ); 
		break; 
	case 133:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 600, 350, _T("那就赶紧应") ); 
		break; 
	case 200:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 600, 350, _T("放弃吧, 你赢不了的") ); 
		break; 
	case 201:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 603, 357, _T("胡说, 认输的应该是你") ); 
		break; 
	case 210:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 600, 350, _T("放弃无谓的抵抗吧") ); 
		break; 
	case 211:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 617, 357, _T("你才是!") ); 
		break; 
	case 400:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 600, 350, _T("看样子要平局了") ); 
		break; 
	case 401:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 600, 357, _T("可累死我了") ); 
		break; 
	case 300:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 602, 350, _T("你水平增长挺快啊") ); 
		break; 
	case 301:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 602, 337, _T("那是自然, 长得帅的人") ); 
		dc.TextOut( 602, 371, _T("一般棋也下的好") ); 
		break; 
	case 600:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 608, 337, _T("阿书, 士别三日") ); 
		dc.TextOut( 608, 371, _T("长进不少啊") ); 
		break; 
	case 601:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 608, 350, _T("师兄承让了^_^") ); 
		break; 
	case 610:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 608, 350, _T("你又输了") ); 
		break; 
	case 611:
		showPaopao( ( CPaintDC& ) dc,  1 ); 
		dc.TextOut( 606, 337, _T("真是的, 师兄一点") ); 
		dc.TextOut( 606, 371, _T("都不手下留情") ); 
		break; 
	case 1000:
		showPaopao( ( CPaintDC& ) dc,  0 ); 
		dc.TextOut( 607, 337, _T("阿书, 再悔棋") ); 
		dc.TextOut( 607, 371, _T("吃了你哦") ); 
		m_TextState++; 
		break; 
	case 1001:
		m_ShowPos  =  true; 
		m_TextState++; 
	default:
		break; 

	}
	
	if( !m_ShowPos )
		return; 

	//显示下棋位置
	if( m_LastStep.x  !=   OUTSCOPE )
	{
		showPaopao( ( CPaintDC& ) dc,  !whichTimer ); 
		CString text; 
		text.Format( "%c%d", 65 + m_LastStep.y, m_LastStep.x+1 ); 
		dc.TextOut( 667, 350, text ); 
	}	
	//
}

void CMainWnd::OnVideo() 
{
	CFileDialog fd( true, "fs", NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		"保存文件( *.fs )|*.fs||", this ); 
	if(  fd.DoModal()  == IDCANCEL  )
		return; 
	
	CString name(  fd.GetPathName()  ); 
	m_Fin.open( name ); 
	m_Fin.clear(); 
	m_Fin>>m_State; 
	
	
	chess = Chess(); 
	chess.begin(); 
	
	SetTimer( 3, 1000, NULL ); 
	m_video  =  true; 
	
}

void CMainWnd::initGame( CPaintDC &dc )
{
	m_WinDC  =  this->GetDC(); 
	SetClassLong( m_hWnd, GCL_HCURSOR, ( LONG )LoadCursor( NULL, IDC_ARROW ) ); 
	dc.FillRect( &CRect( 0, 0, 800, 600 ), &CBrush( RGB( 0, 0, 0 ) ) ); 
	dc.SetTextColor( RGB( 255, 255, 255 ) ); 
	dc.TextOut( 680, 560, "Loading......" );
	//初始化位图
	initBitmap(); 	
	chess = Chess(); 
	//播放背景音乐
	playBGM(); 
	m_SoundOn  =  true; 
	m_Num  =  false; 		 
	m_Time1  =  0; 
	m_Time2  =  0; 
	m_picture  =  NORMAL; 
	m_State  =  STOP; 
	m_video  =  false; 
	m_ShowPos  =  true; 
	m_NumOfBack  =  0; 

}
