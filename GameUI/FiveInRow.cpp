// FiveInRow.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "FiveInRow.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFiveInRowApp

BEGIN_MESSAGE_MAP(CFiveInRowApp, CWinApp)
	//{{AFX_MSG_MAP(CFiveInRowApp)

		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFiveInRowApp construction

CFiveInRowApp::CFiveInRowApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CFiveInRowApp object

CFiveInRowApp theApp;


/////////////////////////////////////////////////////////////////////////////
// CFiveInRowApp initialization

BOOL CFiveInRowApp::InitInstance()
{

	AfxEnableControlContainer();

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	const CString className = _T("FIRClass");
	const int WIDTH = 800;
	const int HEIGHT = 600;


	m_MainWnd = new CMainWnd();
	WNDCLASS wc;
	ZeroMemory(&wc, sizeof(wc));
	wc.hInstance = AfxGetInstanceHandle();
	wc.lpfnWndProc = ::DefWindowProc;
	wc.hbrBackground = NULL;
	wc.hCursor = LoadCursor(IDC_ARROW);
	wc.hIcon = LoadIcon(IDI_ICON);
	wc.lpszClassName = className;
	wc.lpszMenuName = MAKEINTRESOURCE(IDM_MENU);

	
	if(!AfxRegisterClass(&wc))
	{
		AfxMessageBox("注册类失败");
		return FALSE;
	}

	int extraHeight = GetSystemMetrics(SM_CYCAPTION) + GetSystemMetrics(SM_CYMENU);
	int xPos = GetSystemMetrics(SM_CXSCREEN) / 2 - WIDTH / 2;
	int yPos = GetSystemMetrics(SM_CYSCREEN) / 2 - HEIGHT / 2 - extraHeight / 2;

	if( !m_MainWnd->CreateEx(NULL, className, _T("五子棋"), 
							WS_SYSMENU | WS_CAPTION |WS_MINIMIZEBOX ,
							xPos, yPos, WIDTH, HEIGHT + extraHeight,
							NULL, NULL))
	{
		AfxMessageBox(_T("创建主窗口失败"));
		return FALSE;
	}
	
	m_pMainWnd=m_MainWnd;
	
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

