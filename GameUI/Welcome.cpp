// Welcome.cpp : implementation file
//

#include "stdafx.h"
#include "FiveInRow.h"
#include "Welcome.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWelcome dialog


CWelcome::CWelcome(CWnd* pParent /*=NULL*/)
	: CDialog(CWelcome::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWelcome)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CWelcome::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWelcome)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWelcome, CDialog)
	//{{AFX_MSG_MAP(CWelcome)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWelcome message handlers

int CWelcome::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here

	SetWindowPos(NULL,0,0,400,300,SWP_NOMOVE);
	m_WinDC = this->GetDC();
	m_Welcome.LoadBitmap(IDB_WELCOME);
	m_WelcomeDC.CreateCompatibleDC(m_WinDC);
	m_WelcomeDC.SelectObject(&m_Welcome);

	
	return 0;
}

void CWelcome::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here

	dc.StretchBlt(0,0,400,300,&m_WelcomeDC,0,0,800,600,SRCCOPY);
	SetTimer(1,2000,NULL);
	
	// Do not call CDialog::OnPaint() for painting messages
}

void CWelcome::OnDestroy() 
{

	CDialog::OnDestroy();

	// TODO: Add your message handler code here
	
}

void CWelcome::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	OnCancel();
	
	CDialog::OnTimer(nIDEvent);
}
