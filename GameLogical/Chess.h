// Chess.h: interface for the Chess class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHESS_H__7C095EFE_D255_4B08_A44B_2BF8E257F8D8__INCLUDED_)
#define AFX_CHESS_H__7C095EFE_D255_4B08_A44B_2BF8E257F8D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <LIST>
#include <STDLIB.H>
#include <VECTOR>
using namespace std;

//记录空棋位得分情况的结构体
//每个点的得分分为最高分和总分
//最高分相同的情况下比较总分
struct Score
{
	int high;
	int sum;
	Score()
	{
		high = 0;
		sum = 0;
	}
	Score(int h, int s)
	{
		high = h;
		sum = s;
	}
	operator = (const Score& score)
	{
		high = score.high;
		sum = score.sum;
	}
	operator = (const int& n)
	{
		high = n;
		sum = n;
	}
};

class Chess  
{
private:
	enum{BLACK = 0, WHITE = 1, EMPTY = -1, MAXROW = 15, OUTSCOPE = -1,
		ATTACK = 1, DEFENCE = 0 };


	bool m_Begin;						//记录棋局是否开始,1为开始
	bool m_Color;						//记录下一步应该下白子还是黑子,0是黑子,1是白子	
	int m_NumOfSteps;					//记录下棋的步数
	
	int m_ChessArr[MAXROW][MAXROW];		//黑子为BLACK,白子为WHITE
	list<CPoint> m_ChessList;			//记录落子顺序
	
	bool inScope(int i, int j);			//返回数组索引i,j是否越界
	bool oneWin(int color);				//判断某方是否能赢
	
	//
	//人机对战
	//空棋位打分
	Score m_ScoreArr[MAXROW][MAXROW];		//记录每个位置的得分
	vector<CPoint> m_HighList;				//记录得分最高的点的坐标
	bool m_AOrD;//进攻还是防守
	
	void calcScores(bool color);			//为所有位置打分
	void getScore(int x, int y, int color, Score& resultScore);	//对某一位置打分
	int getStyleScore(int count, int weight);	//得到某一棋型的分值

public:
	Chess();
	virtual ~Chess();
	

	void begin();						//开始一局
	void end();							//结束棋局
	 
	bool ifBegin();						//返回棋局是否开始 
	list<CPoint> getChessList();		//返回chesslist
	CPoint getLast();					//返回最后一步的位置
	bool getColor();					//返回下一步应该下白子还是黑子
	 
	bool ifNext(CPoint pos);			//返回在CPoint处能否下子
	void nextStep(CPoint pos);			//增加一步棋
	void backStep();					//悔棋一步
	int getStepNum();					//返回下棋的步数
	bool ifWin();						//判断游戏是否结束
	// 
	//人机对战
	//
	void getNextStep(bool color,CPoint& resultPoint);	//返回下一步的点
	bool attOrDef();									//上步棋是攻击还是防守
	// 
	//博弈树
	void getTreeStep(bool color,CPoint& resultPoint , int n);
	int m_TreeArr[MAXROW][MAXROW];
	int tree(int i, int j, int alpha, int depth, bool color);
};

#endif // !defined(AFX_CHESS_H__7C095EFE_D255_4B08_A44B_2BF8E257F8D8__INCLUDED_)
