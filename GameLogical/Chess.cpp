// Chess.cpp: implementation of the Chess class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FiveInRow.h"
#include "Chess.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Chess::Chess()
{
	m_Color=BLACK;
	m_NumOfSteps=0;
	m_Begin=0;
	for (int i = 0; i < MAXROW; i++)
	{
		for (int j = 0; j < MAXROW; j++)
		{	
			m_ChessArr[i][j] = EMPTY;
		}
	}
}

Chess::~Chess()
{

}

bool Chess::getColor()
{
	return m_Color;
}

bool Chess::ifNext( CPoint pos )
{
	if (m_Begin == 0 || pos.x == OUTSCOPE || pos.y == OUTSCOPE)
		return 0;

	//如果pos处为空 则可以下子
	
	if (m_ChessArr[pos.x][pos.y] == EMPTY)
		return 1;
	else
		return 0;
}

bool Chess::inScope( int i, int j )
{
	return ( i >= 0 && j >= 0 && i < MAXROW && j < MAXROW );
}

void Chess::begin()
{
	m_Begin = true;
}

void Chess::end()
{
	m_Begin = false;
}

bool Chess::ifBegin()
{
	return m_Begin;
}

list<CPoint> Chess::getChessList()
{
	return m_ChessList;
}

CPoint Chess::getLast()
{
	if(m_ChessList.empty())
	{
		return CPoint(OUTSCOPE,OUTSCOPE);
	}
	else
	{
		return m_ChessList.back();
	}
}

void Chess::nextStep( CPoint pos )
{
	m_ChessList.push_back(pos);
	if (m_Color == BLACK)
	{
		m_ChessArr[pos.x][pos.y] = BLACK;
	}
	else
	{
		m_ChessArr[pos.x][pos.y] = WHITE;
	}
	m_Color = !m_Color;
	m_NumOfSteps++;
}

void Chess::backStep()
{
	if(m_NumOfSteps == 0)
		return;

	m_ChessArr[m_ChessList.back().x][m_ChessList.back().y] = EMPTY;
	m_ChessList.pop_back();
	m_Color = !m_Color;
	m_Begin = true;
	m_NumOfSteps--;

}

int Chess::getStepNum()
{
	return m_NumOfSteps;
}

bool Chess::ifWin()
{
	if (m_Color == BLACK)		//最后一步下的是白子
	{
		return oneWin(WHITE);	//判断白子是否能赢
	}
	else						//最后一步下的是黑子
	{
		return oneWin(BLACK);	//判断黑子是否能赢
	}
}

bool Chess::oneWin( int color )
{
	//判断横向是否获胜
	int inRow=1;
	int i=m_ChessList.back().x;
	int j=m_ChessList.back().y;
	while (m_ChessArr[--i][j] == color && inRow != 5 && inScope(i,j))
	{
		inRow++;
	}
	i=m_ChessList.back().x;
	j=m_ChessList.back().y;
	while (m_ChessArr[++i][j] == color && inRow != 5 && inScope(i,j))
	{
		inRow++;
	}
	if (inRow == 5)
	{
		return true;
	}
	else
	{
		inRow=1;
	}

	//判断竖向是否获胜
	i=m_ChessList.back().x;
	j=m_ChessList.back().y;
	while (m_ChessArr[i][--j] == color && inRow != 5 && inScope(i,j))
	{
		inRow++;
	}
	i=m_ChessList.back().x;
	j=m_ChessList.back().y;
	while (m_ChessArr[i][++j] == color && inRow != 5 && inScope(i,j))
	{
		inRow++;
	}
	if (inRow == 5)
	{
		return true;
	}
	else
	{
		inRow = 1;
	}

	//判断捺方向是否获胜
	i=m_ChessList.back().x;
	j=m_ChessList.back().y;
	while (m_ChessArr[--i][--j] == color && inRow != 5 && inScope(i,j))
	{
		inRow++;
	}
	i=m_ChessList.back().x;
	j=m_ChessList.back().y;
	while (m_ChessArr[++i][++j] == color && inRow != 5 && inScope(i,j))
	{
		inRow++;
	}
	if (inRow == 5)
	{
		return true;
	}
	else
	{
		inRow=1;
	}

	//判断撇方向是否获胜
	i=m_ChessList.back().x;
	j=m_ChessList.back().y;
	while (m_ChessArr[--i][++j] == color && inRow != 5 && inScope(i,j))
	{
		inRow++;
	}
	i=m_ChessList.back().x;
	j=m_ChessList.back().y;
	while (m_ChessArr[++i][--j] == color && inRow != 5 && inScope(i,j))
	{
		inRow++;
	}
	if (inRow == 5)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Chess::calcScores( bool color )
{
	for(int i = 0; i < MAXROW; i++)
		for(int j = 0; j < MAXROW; j++)
		{
			if(m_ChessArr[i][j] == EMPTY)
			{
				Score resultPoint1(0,0);
				Score resultPoint2(0,0);
				getScore( i , j , color , resultPoint1 );
				resultPoint1.high += 1;
				getScore( i , j , !color , resultPoint2 );
				(resultPoint1.high > resultPoint2.high)?
					(m_ScoreArr[i][j] = resultPoint1 ):( m_ScoreArr[i][j] = resultPoint2);
			}
			else
			{
				m_ScoreArr[i][j] = 0;
			}
		}	
}

void Chess::getScore( int x, int y, int color, Score& resultScore )
{
	if( m_ChessArr[x][y] != EMPTY )
	{
		resultScore.high = 0;
		return;
	}

	int result[4] = {0};//记录横纵撇捺的得分
	resultScore.sum = 0;
	//横向
	int count=1;
	int weight=0;
	int i=x;
	int j=y;
	while (inScope(--i, j) && m_ChessArr[i][j] == color )
	{
		count++;
	}
	if( inScope(i,j) && m_ChessArr[i][j] == EMPTY)
		weight++;
	i=x;
	j=y;
	while (inScope(++i,j) && m_ChessArr[i][j] == color)
	{
		count++;
	}
	if(inScope(i,j) && m_ChessArr[i][j] == EMPTY)
		weight++;
	
	result[0]=getStyleScore(count, weight);
	resultScore.sum += result[0];
	
	//纵
	count = 1; 
	weight = 0; 
	i = x; 
	j = y;
	while (inScope(i,--j) && m_ChessArr[i][j] == color)
	{
		count++;
	}
	if(inScope(i,j) && m_ChessArr[i][j] == EMPTY)
		weight++;
	i = x; 
	j = y;
	while (inScope(i,++j) && m_ChessArr[i][j] == color)
	{
		count++;
	}
	if(inScope(i,j) && m_ChessArr[i][j] == EMPTY)
		weight++;
	
	result[1]=getStyleScore(count, weight);
	resultScore.sum += result[1];
	
	//撇
	count = 1; 
	weight = 0; 
	i = x; 
	j = y;
	while (inScope(--i,++j) && m_ChessArr[i][j] == color && inScope(i,j))
	{
		count++;
	}
	if(inScope(i,j) && m_ChessArr[i][j] == EMPTY)
		weight++;
	i = x; 
	j = y;
	while (inScope(++i,--j) && m_ChessArr[i][j] == color && inScope(i,j))
	{
		count++;
	}
	if(inScope(i,j) && m_ChessArr[i][j] == EMPTY)
		weight++;
	
	result[2]=getStyleScore(count, weight);
	resultScore.sum += result[2];
	
	//捺
	count = 1; 
	weight = 0; 
	i = x; 
	j = y;
	while (inScope(--i,--j) && m_ChessArr[i][j] == color && inScope(i,j))
	{
		count++;
	}
	if(inScope(i,j) && m_ChessArr[i][j] == EMPTY)
		weight++;
	i = x; 
	j = y;
	while (inScope(++i,++j) && m_ChessArr[i][j] == color && inScope(i,j))
	{
		count++;
	}
	if(inScope(i,j) && m_ChessArr[i][j] == EMPTY)
		weight++;
	
	result[3] = getStyleScore(count, weight);
	resultScore.sum += result[3];
	
	resultScore.high = result[0];
	for(int t = 1; t < 4 ; t++)
	{
		if(result[t] > resultScore.high)
		{
			resultScore.high = result[t];
		}
	}	
}

int Chess::getStyleScore( int count, int weight )
{
	if(count >= 5)
		return 100;					//取胜
	if(weight == 0 || count == 1)
		return 1;					//死棋或者单子
	
	switch(count)
	{
	case 4:
		{		
			switch(weight)
			{
			case 1:
				return 60;//冲四
			case 2:
				return 80;//活四
			}
		}
	case 3:
		{			
			switch(weight)
			{
			case 1:
				return 50;//冲三
			case 2:
				return 70;//活三
			}
		}
	case 2:
		{			
			switch(weight)
			{
			case 1:
				return 10;//冲二
			case 2:
				return 40;//活二
			}
		}
	default:
		return 999;		//错误
	}	
}

void Chess::getNextStep( bool color,CPoint& resultPoint )
{
	int maxScore = 0;									//最高分
	int maxSumScore = 0;								//最高总分
	calcScores(color);
	for(int i = 0; i < MAXROW; i++)
		for(int j = 0; j < MAXROW; j++)
		{
			if(m_ScoreArr[i][j].high > maxScore)		//记录最高分
			{
				maxScore = m_ScoreArr[i][j].high ;
				m_HighList.clear();
				m_HighList.push_back(CPoint(i,j));

				if( m_ScoreArr[i][j].high % 2 == 1 )
					m_AOrD = ATTACK;
				else
					m_AOrD = DEFENCE;
			}
			else if (m_ScoreArr[i][j].high == maxScore)	//最高分相同则比较总分
			{
				if (!m_HighList.empty())
				{
					if(m_ScoreArr[i][j].high > maxSumScore)
					{
						maxSumScore = m_ScoreArr[i][j].sum;
						m_HighList.clear();
						m_HighList.push_back(CPoint(i,j));
					}
					else if(m_ScoreArr[i][j].sum == maxSumScore)
					{
						m_HighList.push_back(CPoint(i,j));
					}
				}
			}
		}
		
		int hSize = m_HighList.size();
		if(hSize==0)						//最高分为0则在中间下子
		{
			resultPoint.x=7;
			resultPoint.y=7;
		}
		else if(hSize==1)					//只有一个最高分则在该点下子
		{
			resultPoint=m_HighList[0];
		}
		else
		{
			srand(time(NULL));				//有多个相同最高分点随机取一点
			int r = rand() % hSize;
			resultPoint = m_HighList[r];
		}


}

bool Chess::attOrDef()
{
	return m_AOrD;
}

void Chess::getTreeStep( bool color,CPoint& resultPoint, int n)
{

	{
		for(int i = 0; i < MAXROW ; i++)
		{
			for(int j = 0; j < MAXROW ; j++)
			{
				int alpha = 0;
				m_TreeArr[i][j] = tree( i , j, alpha , n , m_Color);
			}
		}
	}
	
	int maxScore = 0;
	for(int i = 0 ; i < MAXROW; i++)
	{
		for(int j = 0 ; j < MAXROW; j++)
		{
			if(m_TreeArr[i][j] > maxScore)		//记录最高分
			{
				maxScore = m_TreeArr[i][j] ;
				m_HighList.clear();
				m_HighList.push_back(CPoint(i,j));
			}
			else if(m_TreeArr[i][j] == maxScore)
			{
				m_HighList.push_back(CPoint(i,j));
			}
		}
	}

		int hSize = m_HighList.size();
		if(hSize==0)						//最高分为0则在中间下子
		{
			resultPoint.x=7;
			resultPoint.y=7;
		}
		else if(hSize==1)					//只有一个最高分则在该点下子
		{
			resultPoint=m_HighList[0];
		}
		else
		{
			srand(time(NULL));				//有多个相同最高分点随机取一点
			int r = rand() % hSize;
			resultPoint = m_HighList[r];
		}
}
//返回(i,j)点的打分
int Chess::tree( int i, int j, int alpha, int depth, bool color )
{
	if( alpha == 100)
		return 100;
	

	if( m_ChessArr[i][j] == EMPTY)
	{
		m_ChessArr[i][j] = color;//假设电脑落点
		//CPoint anotherStep;
		//getTreeStep( !color , anotherStep , depth );
		//m_ChessArr[anotherStep.x][anotherStep.y] = !color;//玩家落点

		if( depth < 1 )
		{
			if(m_ChessArr[i][j] == EMPTY)
			{
				Score resultPoint1(0,0);
				Score resultPoint2(0,0);
				getScore( i , j , color , resultPoint1 );
				resultPoint1.high += 1;
				getScore( i , j , !color , resultPoint2 );
				(resultPoint1.high > resultPoint2.high)?
					(alpha = resultPoint1.high ):( alpha = resultPoint2.high );
			}
			else
			{
				alpha = 0;
			}
			return alpha;
		}

		depth--;

		for(int m = 0 ; m < MAXROW; m++)
		{
			for(int n = 0 ; n < MAXROW; n++)
			{
				alpha = max( alpha , tree( m , n , alpha , depth, color ) );				
			}
		}
		m_ChessArr[i][j] =  EMPTY;
		//m_ChessArr[anotherStep.x][anotherStep.y] = EMPTY;
		return alpha;
	}
	return 0;
	
}


