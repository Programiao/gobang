//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by FiveInRow.rc
//
#define IDD_ABOUTBOX                    100
#define IDB_SPLASH1                     104
#define IDR_MAINFRAME                   128
#define IDI_ICON                        128
#define IDM_MENU                        128
#define IDR_FIVEINTYPE                  129
#define IDB_BG                          130
#define IDB_T1                          131
#define IDB_GZ                          132
#define IDB_BLACK                       133
#define IDB_WHITE                       134
#define IDB_PAO1                        135
#define IDB_PAO2                        136
#define IDB_MASK                        137
#define IDB_GMASK                       138
#define IDB_PAOMASK1                    139
#define IDB_PAOMASK2                    140
#define IDC_BLACK                       141
#define IDC_WHITE                       142
#define IDW_BACK                        145
#define IDW_NEXT                        146
#define IDB_SPLASH                      152
#define IDB_WIN                         153
#define IDB_SURRENDER                   154
#define IDB_LOSE                        155
#define IDB_WELCOME                     156
#define IDD_WELCOME                     157
#define IDW_WIN                         158
#define IDW_LOSE                        159
#define IDB_ABOUTUS                     160
#define IDB_HELP                        161
#define IDD_DIALOG1                     162
#define IDD_DIALOG2                     163
#define IDC_CURSOR1                     164
#define ID_MENUITEM32771                32771
#define IDM_TOPEOPLE                    32771
#define IDM_BLACK                       32773
#define IDM_WHITE                       32774
#define IDM_BACKSTEP                    32775
#define IDM_TISHI                       32776
#define IDM_SURRENDER                   32777
#define IDM_EXIT                        32778
#define IDM_HELP                        32779
#define IDM_ABOUTUS                     32780
#define IDM_NUM                         32781
#define IDM_SAVE                        32782
#define IDM_SAVEFILE                    32782
#define IDM_LOAD                        32783
#define IDM_LOADFILE                    32783
#define IDM_BGM                         32784
#define IDM_SOUND                       32787
#define IDM_BGM1                        32791
#define IDM_BGM2                        32792
#define IDM_BGM3                        32793
#define IDM_BGM4                        32794
#define IDM_BGM5                        32795
#define IDM_VIDEO                       32796

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        166
#define _APS_NEXT_COMMAND_VALUE         32797
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
